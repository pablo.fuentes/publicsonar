FROM golang:alpine AS builder

WORKDIR /app/

COPY service.go .

RUN apk add git

RUN go get github.com/globalsign/mgo &&  go install github.com/globalsign/mgo

RUN go build -o main .


FROM alpine:latest

COPY  --from=builder /app .

ARG mongo_url=localhost:27017/devops

CMD ["./main"]

