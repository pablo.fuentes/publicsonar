#Public Sonar Go API
 
This repository contains a go api with mongodb. You can run it using docker (with compose) or kubernetes 
 
## Docker 

You can run as a stand alone docker container, but best is to use docker compose. docker compose will 
make the api and mongodb up un running just typing ``docker-compose up``
the api will be available in port 8080, also if you need to do any operational work inside the mongo, it is running on port 27017

## kubernetes 

For having it running with kubernetes , you will need to apply the secrets and then the services
`kubectl apply -f secrets/mongo_hosts.yml` and then ``kubectl apply -f deploy/ `` , the api needs some minutes to connect to mongodb            
